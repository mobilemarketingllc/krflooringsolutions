<?php get_header(); ?>
<?php include( dirname( __FILE__ ) . '../include/constant.php' );?>
<?php /*?>
<div class="container">
    <div class="row">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/" style="padding-top: 20px; padding-left: 20px;">
             <a href="/">Home</a> &raquo; <a href="/flooring/">Flooring</a> &raquo; <a href="/flooring/luxury-vinyl/">Luxury Vinyl</a> &raquo; <a href="/flooring/luxury-vinyl/products/">Luxury Vinyl Products</a> &raquo; <?php the_title(); ?>
    </div>
        </div>
</div>
<?php*/ ?>
<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12 <?php //FLTheme::content_class(); ?>">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php 
				if($LAYOUT_COL == 0){
					if(get_field('gallery_room_images')){
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product.php';
					}else{
						
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-5.php';
					}
				}
				else{
					if(get_field('gallery_room_images')){

					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-'.$LAYOUT_COL.'.php';

					}else{

						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-5.php';
					}
				}
				include( $dir ); 
			?>
				<?php //get_template_part('content', 'single-product'); ?>
			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>
	</div>
</div>

<?php get_footer(); ?>